import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { EmpleadoPuesto } from '../models/ep';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EpService {
  environmentUrl;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {
    this.environmentUrl = environment.url + 'ep';
  }

  getEPs(): Observable<EmpleadoPuesto[]> {
    return this.http
      .get(this.environmentUrl)
      .pipe(map((response) => response as EmpleadoPuesto[]));
  }

  getEP(id: number): Observable<EmpleadoPuesto> {
    return this.http
      .get<EmpleadoPuesto>(`${this.environmentUrl}/${id}`)
      .pipe(map((response) => response));
  }

  create(ep: EmpleadoPuesto): Observable<EmpleadoPuesto> {
    return this.http.post<EmpleadoPuesto>(this.environmentUrl, ep, {
      headers: this.httpHeaders,
    });
  }

  update(puesto: EmpleadoPuesto): Observable<EmpleadoPuesto> {
    return this.http.put<EmpleadoPuesto>(
      `${this.environmentUrl}/${puesto.id}`,
      puesto
    );
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(this.environmentUrl + '/' + id);
  }
}
