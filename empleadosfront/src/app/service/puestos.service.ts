import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Puesto } from '../models/Puesto';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PuestosService {
  environmentUrl;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {
    this.environmentUrl = environment.url + 'puestos';
  }

  getPuestos(): Observable<Puesto[]> {
    return this.http
      .get(this.environmentUrl)
      .pipe(map((response) => response as Puesto[]));
  }

  getPuesto(id: number): Observable<Puesto> {
    return this.http
      .get<Puesto>(`${this.environmentUrl}/${id}`)
      .pipe(map((response) => response));
  }

  create(puesto: Puesto): Observable<Puesto> {
    return this.http.post<Puesto>(this.environmentUrl, puesto, {
      headers: this.httpHeaders,
    });
  }

  update(puesto: Puesto): Observable<Puesto> {
    return this.http.put<Puesto>(`${this.environmentUrl}/${puesto.id}`, puesto);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(this.environmentUrl + '/' + id);
  }
}
