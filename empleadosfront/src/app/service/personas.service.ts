import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Persona } from '../models/Personas';

@Injectable({
  providedIn: 'root',
})
export class PersonasService {
  environmentUrl;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) {
    this.environmentUrl = environment.url + 'personas';
  }

  getPersonas(): Observable<Persona[]> {
    return this.http
      .get(this.environmentUrl)
      .pipe(map((response) => response as Persona[]));
  }

  getPersona(id: number): Observable<Persona> {
    return this.http
      .get<Persona>(`${this.environmentUrl}/${id}`)
      .pipe(map((response) => response));
  }

  create(persona: Persona): Observable<Persona> {
    return this.http.post<Persona>(this.environmentUrl, persona, {
      headers: this.httpHeaders,
    });
  }

  update(persona: Persona): Observable<Persona> {
    return this.http.put<Persona>(
      `${this.environmentUrl}/${persona.id}`,
      persona
    );
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(this.environmentUrl + '/' + id);
  }
}
