import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

import { EmpleadoPuesto } from '../models/ep';
import { Persona } from '../models/Personas';
import { Puesto } from '../models/Puesto';
import { EpService } from '../service/ep.service';
import { PersonasService } from '../service/personas.service';
import { PuestosService } from '../service/puestos.service';

@Component({
  selector: 'app-ep-form',
  templateUrl: './ep-form.component.html',
})
export class EpFormComponent implements OnInit {
  ep: EmpleadoPuesto = new EmpleadoPuesto();
  personas: Persona[];
  puestos: Puesto[];

  constructor(
    private epService: EpService,
    private puestosService: PuestosService,
    private personasService: PersonasService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarEP();
    this.cargarPuestos();
    this.cargarPersonas();
  }

  cargarEP(): void {
    this.activatedRoute.params.subscribe((params) => {
      let id = params['id'];
      if (id) {
        this.epService.getEP(id).subscribe((ep) => {
          this.ep = ep;
        });
      }
    });
  }

  cargarPersonas() {
    this.personasService.getPersonas().subscribe((res) => {
      this.personas = res;
    });
  }

  cargarPuestos() {
    this.puestosService.getPuestos().subscribe((res) => {
      this.puestos = res;
    });
  }

  create(): void {
    this.epService.create(this.ep).subscribe((res) => {
      console.log(res);

      Swal.fire('Nuevo Empleado Puesto!', `creado exitosamente`, 'success');

      this.router.navigate(['/ep']);
    });
  }

  update(): void {
    this.epService.update(this.ep).subscribe((ep) => {
      this.router.navigate(['/ep']);
      Swal.fire(
        'Empleado persona actualizada!',
        `${this.ep.id} actualizado exitosamente`,
        'success'
      );
    });
  }

  compararPuesto(o1: Puesto, o2: Puesto): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }
    return o1 === null || o2 === null || o1 === undefined || o2 === undefined
      ? false
      : o1.id === o2.id;
  }

  compararPersona(o1: Puesto, o2: Puesto): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }
    return o1 === null || o2 === null || o1 === undefined || o2 === undefined
      ? false
      : o1.id === o2.id;
  }
}
