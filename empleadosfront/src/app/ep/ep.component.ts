import { Component, OnInit } from '@angular/core';
import { EmpleadoPuesto } from '../models/ep';
import { Subject } from 'rxjs';
import { EpService } from '../service/ep.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ep',
  templateUrl: './ep.component.html',
})
export class EpComponent implements OnInit {
  ep: EmpleadoPuesto[];

  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private epService: EpService) {}

  ngOnInit(): void {
    this.epService.getEPs().subscribe((res) => {
      this.ep = res;
      this.dtTrigger.next();
      console.log(res);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  delete(ep: EmpleadoPuesto): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.epService.delete(ep.id).subscribe(
          (response) => {
            this.ep = this.ep.filter((cli) => cli !== ep);
            Swal.fire(
              'Deleted!',
              `Empleado Puesto ID: ${ep.id} eliminado`,
              'success'
            );
          },
          (err) => {
            Swal.fire('Error!', `Error ${err}`, 'error');
          }
        );
      }
    });
  }
}
