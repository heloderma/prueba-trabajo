import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { DataTablesModule } from 'angular-datatables';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { EpComponent } from './ep/ep.component';
import { FormComponent } from './personas/form.component';
import { PersonasComponent } from './personas/personas.component';
import { PuestosComponent } from './puestos/puestos.component';
import { PuestosFormComponent } from './puestos/puestos-form.component';
import { EpFormComponent } from './ep/ep-form.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    PuestosComponent,
    EpComponent,
    HeaderComponent,
    FormComponent,
    PuestosFormComponent,
    EpFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DataTablesModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
