import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonasComponent } from './personas/personas.component';
import { PuestosComponent } from './puestos/puestos.component';
import { EpComponent } from './ep/ep.component';
import { FormComponent } from './personas/form.component';
import { PuestosFormComponent } from './puestos/puestos-form.component';
import { EpFormComponent } from './ep/ep-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/personas', pathMatch: 'full' },
  { path: 'personas', component: PersonasComponent },
  { path: 'personas/form', component: FormComponent },
  { path: 'personas/form/:id', component: FormComponent },
  { path: 'puestos', component: PuestosComponent },
  { path: 'puestos/form', component: PuestosFormComponent },
  { path: 'puestos/form/:id', component: PuestosFormComponent },
  { path: 'ep', component: EpComponent },
  { path: 'ep/form', component: EpFormComponent },
  { path: 'ep/form/:id', component: EpFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
