import { Puesto } from './Puesto';
import { Persona } from './Personas';
export class EmpleadoPuesto {
  id: number;
  puesto: Puesto;
  persona: Persona;
}
