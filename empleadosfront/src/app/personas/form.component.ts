import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona } from '../models/Personas';
import { PersonasService } from '../service/personas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {
  persona: Persona = new Persona();

  constructor(
    private personasService: PersonasService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarPersona();
  }

  cargarPersona(): void {
    this.activatedRoute.params.subscribe((params) => {
      let id = params['id'];
      if (id) {
        this.personasService
          .getPersona(id)
          .subscribe((persona) => (this.persona = persona));
      }
    });
  }

  create(): void {
    this.personasService.create(this.persona).subscribe((res) => {
      console.log(res);

      Swal.fire(
        'Nueva Persona!',
        `${this.persona.nombre} creada exitosamente`,
        'success'
      );

      this.router.navigate(['/personas']);
    });
  }

  update(): void {
    this.personasService.update(this.persona).subscribe((persona) => {
      this.router.navigate(['/personas']);
      Swal.fire(
        'Persona actualizada!',
        `${this.persona.nombre} actualizada exitosamente`,
        'success'
      );
    });
  }
}
