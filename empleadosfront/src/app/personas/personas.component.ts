import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';

import { Persona } from '../models/Personas';
import { PersonasService } from '../service/personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
})
export class PersonasComponent implements OnInit {
  personas: Persona[];

  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private personasService: PersonasService) {}

  ngOnInit(): void {
    this.personasService.getPersonas().subscribe((res) => {
      this.personas = res;
      this.dtTrigger.next();
      console.log(res);
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  delete(persona: Persona): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.personasService.delete(persona.id).subscribe(
          (response) => {
            this.personas = this.personas.filter((cli) => cli !== persona);
            Swal.fire(
              'Deleted!',
              `Persona ${persona.nombre} eliminado`,
              'success'
            );
          },
          (err) => {
            Swal.fire(
              'Error!',
              `No se puede eliminar a la persona ${persona.nombre} primero eliminalo de la tabla Empleados Puestos`,
              'error'
            );
          }
        );
      }
    });
  }
}
