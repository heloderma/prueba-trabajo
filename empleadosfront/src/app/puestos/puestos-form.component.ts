import { Component, OnInit } from '@angular/core';
import { Puesto } from '../models/Puesto';
import { PuestosService } from '../service/puestos.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-puestos-form',
  templateUrl: './puestos-form.component.html',
})
export class PuestosFormComponent implements OnInit {
  puesto: Puesto = new Puesto();

  constructor(
    private puestosService: PuestosService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.cargarPuestos();
  }

  cargarPuestos(): void {
    this.activatedRoute.params.subscribe((params) => {
      let id = params['id'];
      if (id) {
        this.puestosService
          .getPuesto(id)
          .subscribe((puesto) => (this.puesto = puesto));
      }
    });
  }

  create(): void {
    this.puestosService.create(this.puesto).subscribe((res) => {
      console.log(res);

      Swal.fire(
        'Nuevo Puesto!',
        `${this.puesto.nombre} creado exitosamente`,
        'success'
      );

      this.router.navigate(['/puestos']);
    });
  }

  update(): void {
    this.puestosService.update(this.puesto).subscribe((puesto) => {
      this.router.navigate(['/puestos']);
      Swal.fire(
        'Puesto actualizado!',
        `${this.puesto.nombre} actualizado exitosamente`,
        'success'
      );
    });
  }
}
