import { Component, OnInit } from '@angular/core';
import { Puesto } from '../models/Puesto';
import { Subject } from 'rxjs';
import { PuestosService } from '../service/puestos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-puestos',
  templateUrl: './puestos.component.html',
})
export class PuestosComponent implements OnInit {
  puestos: Puesto[];

  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private puestosService: PuestosService) {}

  ngOnInit(): void {
    this.puestosService.getPuestos().subscribe((res) => {
      this.puestos = res;
      this.dtTrigger.next();
      console.log(res);
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  delete(puesto: Puesto): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.puestosService.delete(puesto.id).subscribe(
          (response) => {
            this.puestos = this.puestos.filter((cli) => cli !== puesto);
            Swal.fire(
              'Deleted!',
              `Puesto ${puesto.nombre} eliminado`,
              'success'
            );
          },
          (err) => {
            Swal.fire(
              'Error!',
              `No se puede eliminar el puesto ${puesto.nombre} primero eliminalo de la tabla Empleados Puestos`,
              'error'
            );
          }
        );
      }
    });
  }
}
