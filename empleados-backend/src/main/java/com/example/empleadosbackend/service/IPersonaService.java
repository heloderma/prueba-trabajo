package com.example.empleadosbackend.service;

import com.example.empleadosbackend.entity.Persona;

import java.util.List;

public interface IPersonaService {
    List<Persona> findAll();

    Persona findById(Long id);

    Persona save(Persona persona);

    void delete(Long id);
}
