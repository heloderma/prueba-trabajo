package com.example.empleadosbackend.service;

import com.example.empleadosbackend.dao.IEmpleadoPuestoDao;
import com.example.empleadosbackend.entity.EmpleadoPuesto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpleadoPuestoServiceImpl implements IEmpleadoPuestoService{
    private IEmpleadoPuestoDao epDao;
    public EmpleadoPuestoServiceImpl(IEmpleadoPuestoDao epDao) {
        this.epDao = epDao;
    }

    @Override
    public List<EmpleadoPuesto> findAll() {
        return (List<EmpleadoPuesto>) epDao.findAll();
    }

    @Override
    public EmpleadoPuesto findById(Long id) {
        return epDao.findById(id).orElse(null);
    }

    @Override
    public EmpleadoPuesto save(EmpleadoPuesto persona) {
        return epDao.save(persona);
    }

    @Override
    public void delete(Long id) {
        epDao.deleteById(id);
    }
}
