package com.example.empleadosbackend.service;

import com.example.empleadosbackend.dao.IPersonaDao;
import com.example.empleadosbackend.entity.Persona;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonaServiceImpl implements IPersonaService{
    private IPersonaDao personaDao;
    public PersonaServiceImpl(IPersonaDao personaDao) {
        this.personaDao = personaDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Persona> findAll() {
        return (List<Persona>) personaDao.findAll();
    }

    @Override
    public Persona findById(Long id) {
        return personaDao.findById(id).orElse(null);
    }

    @Override
    public Persona save(Persona persona) {
        return personaDao.save(persona);
    }

    @Override
    public void delete(Long id) {
        personaDao.deleteById(id);
    }
}
