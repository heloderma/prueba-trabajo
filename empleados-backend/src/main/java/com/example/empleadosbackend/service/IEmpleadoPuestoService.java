package com.example.empleadosbackend.service;

import com.example.empleadosbackend.entity.EmpleadoPuesto;

import java.util.List;

public interface IEmpleadoPuestoService {
    List<EmpleadoPuesto> findAll();

    EmpleadoPuesto findById(Long id);

    EmpleadoPuesto save(EmpleadoPuesto persona);

    void delete(Long id);
}
