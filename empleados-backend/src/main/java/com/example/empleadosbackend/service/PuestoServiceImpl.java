package com.example.empleadosbackend.service;

import com.example.empleadosbackend.dao.IPuestoDao;
import com.example.empleadosbackend.entity.Puesto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PuestoServiceImpl implements IPuestoService{
    private IPuestoDao puestoDao;
    public PuestoServiceImpl(IPuestoDao puestoDao) {
        this.puestoDao = puestoDao;
    }

    @Override
    public List<Puesto> findAll() {
        return (List<Puesto>) puestoDao.findAll();
    }

    @Override
    public Puesto findById(Long id) {
        return puestoDao.findById(id).orElse(null);
    }

    @Override
    public Puesto save(Puesto puesto) {
        return puestoDao.save(puesto);
    }

    @Override
    public void delete(Long id) {
        puestoDao.deleteById(id);
    }


}
