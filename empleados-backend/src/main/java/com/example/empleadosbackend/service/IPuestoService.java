package com.example.empleadosbackend.service;

import com.example.empleadosbackend.entity.Puesto;

import java.util.List;

public interface IPuestoService {
    List<Puesto> findAll();

    Puesto findById(Long id);

    Puesto save(Puesto puesto);

    void delete(Long id);
}
