package com.example.empleadosbackend.dao;

import com.example.empleadosbackend.entity.Puesto;
import org.springframework.data.repository.CrudRepository;

public interface IPuestoDao extends CrudRepository<Puesto, Long> {
}
