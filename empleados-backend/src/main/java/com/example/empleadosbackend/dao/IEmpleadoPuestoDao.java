package com.example.empleadosbackend.dao;

import com.example.empleadosbackend.entity.EmpleadoPuesto;
import org.springframework.data.repository.CrudRepository;

public interface IEmpleadoPuestoDao extends CrudRepository<EmpleadoPuesto, Long> {
}
