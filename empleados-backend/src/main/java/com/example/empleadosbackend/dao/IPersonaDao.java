package com.example.empleadosbackend.dao;

import com.example.empleadosbackend.entity.Persona;
import org.springframework.data.repository.CrudRepository;

public interface IPersonaDao extends CrudRepository<Persona, Long> {
}
