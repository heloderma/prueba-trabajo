package com.example.empleadosbackend.controller;

import com.example.empleadosbackend.entity.EmpleadoPuesto;
import com.example.empleadosbackend.entity.Persona;
import com.example.empleadosbackend.entity.Puesto;
import com.example.empleadosbackend.service.IEmpleadoPuestoService;
import com.example.empleadosbackend.service.IPersonaService;
import com.example.empleadosbackend.service.IPuestoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class AppRestController {
    private final IPersonaService personaService;
    private final IPuestoService puestoService;
    private final IEmpleadoPuestoService EPService;

    public AppRestController(IPersonaService personaService, IPuestoService puestoService, IEmpleadoPuestoService EPService) {
        this.personaService = personaService;
        this.puestoService = puestoService;
        this.EPService = EPService;
    }

    @GetMapping("/personas")
    public List<Persona> personas() {
        return personaService.findAll();
    }

    @GetMapping("/personas/{id}")
    public Persona showPersonas(@PathVariable Long id) {
        return personaService.findById(id);
    }

    @PostMapping("/personas")
    @ResponseStatus(HttpStatus.CREATED)
    public Persona createPersona(@RequestBody Persona persona) {
        return personaService.save(persona);
    }

    @PutMapping("/personas/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Persona updatePersona(@RequestBody Persona persona, @PathVariable Long id) {
        Persona personaActual = personaService.findById(id);
        personaActual.setNombre(persona.getNombre());
        personaActual.setApellido(persona.getApellido());
        personaActual.setFechaNacimiento(persona.getFechaNacimiento());

        return personaService.save(personaActual);
    }

    @DeleteMapping("/personas/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePersona(@PathVariable Long id) {
        personaService.delete(id);
    }

    //    PUESTOS
    @GetMapping("/puestos")
    public List<Puesto> puestos() {
        return puestoService.findAll();
    }

    @GetMapping("/puestos/{id}")
    public Puesto showPuesto(@PathVariable Long id) {
        return puestoService.findById(id);
    }

    @PostMapping("/puestos")
    @ResponseStatus(HttpStatus.CREATED)
    public Puesto createPuesto(@RequestBody Puesto puesto) {
        return puestoService.save(puesto);
    }

    @PutMapping("/puestos/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Puesto updatePuesto(@RequestBody Puesto puesto, @PathVariable Long id) {
        Puesto puestoActual = puestoService.findById(id);

        puestoActual.setNombre(puesto.getNombre());

        return puestoService.save(puestoActual);
    }

    @DeleteMapping("/puestos/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePuesto(@PathVariable Long id) {
        puestoService.delete(id);
    }

    //    EMPLEADO PUESTOS
    @GetMapping("/ep")
    public List<EmpleadoPuesto> empleadoPuestos() {
        return EPService.findAll();
    }

    @GetMapping("/ep/{id}")
    public EmpleadoPuesto showEmpleadoPuesto(@PathVariable Long id) {
        return EPService.findById(id);
    }

    @PostMapping("/ep")
    @ResponseStatus(HttpStatus.CREATED)
    public EmpleadoPuesto createEmpleadoPuesto(@RequestBody EmpleadoPuesto ep) {
        return EPService.save(ep);
    }

    @PutMapping("/ep/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public EmpleadoPuesto updateEmpleadoPuesto(@RequestBody EmpleadoPuesto ep, @PathVariable Long id) {
        EmpleadoPuesto empleadoPuestoActual = EPService.findById(id);

        empleadoPuestoActual.setPuesto(ep.getPuesto());
        empleadoPuestoActual.setPersona(ep.getPersona());

        return EPService.save(empleadoPuestoActual);
    }

    @DeleteMapping("/ep/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmpleadoPuesto(@PathVariable Long id) {
        EPService.delete(id);
    }

}
