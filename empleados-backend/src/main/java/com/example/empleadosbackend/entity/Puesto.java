package com.example.empleadosbackend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class Puesto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

//    @OneToMany(mappedBy = "puesto")
//    Set<EmpleadoPuesto> empleadoPuesto;

    private static final long serialVersionUID = -6057397713104623049L;
}
