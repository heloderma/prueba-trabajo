package com.example.empleadosbackend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class EmpleadoPuesto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "persona")
    Persona persona;

    @ManyToOne
    @JoinColumn(name = "puesto")
    Puesto puesto;

    private static final long serialVersionUID = -7871125875890069774L;

}
