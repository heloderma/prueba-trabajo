INSERT INTO persona (nombre, apellido, fecha_nacimiento) VALUES ('Andrea', 'Abreu', '1999-08-01');
INSERT INTO persona (nombre, apellido, fecha_nacimiento) VALUES ('Liliana', 'Zurita', '1999-05-13');

INSERT INTO puesto (nombre) VALUES ('Abogado');
INSERT INTO puesto (nombre) VALUES ('Economista');

INSERT INTO empleado_puesto (persona,puesto) VALUES (1,1);
INSERT INTO empleado_puesto (persona,puesto) VALUES (2,2);
